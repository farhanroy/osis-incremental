let count1 = document.getElementById("count1");
let count2 = document.getElementById("count2");

let jumlahCount1 = 0;
let jumlahCount2 = 0;
updateDisplay();

window.addEventListener("keypress", (e) => {
    if(e.key == "w" || e.key == "W") {
        jumlahCount1++;
        updateDisplay();
    } else if (e.key == "s" || e.key == "S") {
        jumlahCount1--;
        updateDisplay();
    }
}); 

window.addEventListener("keypress", (e) => {
    if(e.key == "o" || e.key == "O") {
        jumlahCount2++;
        updateDisplay();
    } else if (e.key == "l" || e.key == "L") {
        jumlahCount2--;
        updateDisplay();
    }
});

function updateDisplay() {
    count1.innerHTML = jumlahCount1;
    count2.innerHTML = jumlahCount2;
}

